# A two-piece brace assembly to attach a car rooftop box to the beams
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq
from math import *

hole_distance=69
height=10
width=20
margin=10
length=hole_distance+2*margin
corner_radius=3
chamfer_size=0.5
head_radius=9.5
head_height=5
nut_hole_d=10.2*sqrt(3)/2. # circumscribed circle diameter for M6 hexagonal nut head
hole_d=6.2                 # hole diameter for M6 bolt shaf
doSkirt=False
skirt_gap=4
skirt_width=0.6

# top bar
t=cq.Workplane("XY").rect(length,width).extrude(height)
t=t.edges("|Z").fillet(corner_radius)
t=t.edges("#Z").chamfer(chamfer_size)
d=0.5*hole_distance
pts=[(d,0),(-d,0)]
t=t.faces(">Z").workplane().pushPoints(pts).circle(head_radius).extrude(head_height)
t=t.pushPoints(pts).circle(0.5*hole_d).cutThruAll()
t=t.pushPoints(pts).polygon(6,nut_hole_d).cutBlind(head_height)
s = cq.selectors.StringSyntaxSelector
t=t.edges(s(">Z")+s("<Z")).chamfer(chamfer_size)





# bottom bar
b=cq.Workplane("XY").rect(length,width).extrude(height)
b=b.edges("|Z").fillet(corner_radius)
d=0.5*hole_distance
pts=[(d,0),(-d,0)]
b=b.faces(">Z").workplane().pushPoints(pts).circle(0.5*hole_d).cutThruAll()
b=b.faces("#Z").edges().chamfer(chamfer_size)
#n=12
#delta=360/6 
#for i in range(n):
#  a=i*delta # angle
#  l=b.moveTo([-d,0]).line(lined*cos(a),lined*sin(a)).extrude(line_height)


def skirt(h):
  sk=cq.Workplane("XY").rect(length+skirt_gap,width+skirt_gap).extrude(h)
  sk=sk.edges("|Z").fillet(corner_radius+skirt_gap)
  sk=sk.faces(s(">Z")+s("<Z")).shell(-skirt_width)
  return sk



if doSkirt:
  b=b.union(skirt(height))
  t=t.union(skirt(height+head_height))


#show_object(b)

# Now the lock

w=2 # lock wall thickness
l_ofs=7 # lock offset from the base plane
l_height=4
l_chamfer=0.2
l_tol=0.5
l_hook=2
l_length=15
eps=0.1
# a hollow cylinder
l=cq.Workplane("XY").workplane(offset=l_ofs+height).circle(0.5*hole_d+w).extrude(l_height)
l=l.circle(0.5*hole_d).cutThruAll()
# horizontal connections
l1=l.moveTo(l_length,-0.5*hole_d).line(-2*l_length,0).line(0,-w).line(2*l_length,0).close().extrude(l_height,False)
l=l.union(l1).union(l1.mirror("XZ"))
#l=l.faces(s(">Z")+s("<Z")).chamfer(l_chamfer)
# rear part with locks
l2=cq.Workplane("YZ").workplane(offset=-margin).moveTo(0.5*hole_d+w,height+l_height+l_ofs).lineTo(0.5*width-eps,height+l_height+l_ofs)
l2=l2.line(w,-w).lineTo(0.5*width+w,-2*l_hook-l_tol).line(-w,0).line(-l_hook,l_hook).line(l_hook,l_hook)
l2=l2.lineTo(0.5*width-eps,l_ofs+l_height+height-2*w).line(-w,w).lineTo(0.5*hole_d+w,l_height+height-w+l_ofs).close().extrude(margin+0.5*hole_d)
#l=l.faces(s("<X")+s(">X")).edges().chamfer(0.3)
l=l.union(l2).union(l2.mirror("XZ"))
l=l.faces(s(">Z")+s("<X")+s("#X")).edges().chamfer(0.5)

def skirt2(h):
  sk=cq.Workplane("XY").workplane(offset=-2*l_hook-l_tol).rect(2*l_length+skirt_gap,width+2*w+skirt_gap).extrude(h)
  sk=sk.edges("|Z").fillet(corner_radius+skirt_gap)
  sk=sk.faces(s(">Z")+s("<Z")).shell(-skirt_width)
  return sk

if doSkirt:
  l=l.union(skirt2(l_ofs+l_height+height+2*l_hook+l_tol))



#show_object(l)
#show_object(l.translate((-0.5*hole_distance,0,0)))
show_object(b)
show_object(t)

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(t,"rbtop.stl")
write_object(b,"rbbottom.stl")
write_object(l,"rblock.stl")
