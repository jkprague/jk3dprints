# Plug for a bike stand
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq
from math import *

hole_d=9 # hole diameter
tube_h=20 # tube height
#tube_h2=0.4*tube_h # loft up to this height
wall=2   # wall thickness
rim=0
width=hole_d+2*wall+rim
length=2*width
corner_r=2
chamfer_r=0.5
ofs=10 # how much to shift the tube up from the baseplane
height=3 # bottom plate height
# create the base plane
#pts=[(-0.5*length,-0.5*width),(0,-0.5*width),(0.5*length,-0.5*width),(0.5*length,0),
#    (0.5*length,0.5*width),(0,0.5*width),(-0.5*length,0.5*width),(-0.5*length,0),(-0.5*length,-0.5*width)]
pts=[(-0.5*length,0),(-0.5*length,-0.25*width),(-0.5*length,-0.5*width),(0*length,-0.5*width),
       (0.5*length,-0.05*width),(0.5*length,0.05*width),(0*length,0.5*width),
       (-0.5*length,0.5*width),(-0.5*length,0.25*width),(-0.5*length,0)]

t=cq.Workplane("XY").workplane().spline(pts).close()
#t=t.faces(">Z").workplane().center(0,0).spline(pts).close()
#t=t.extrude(height)
# extended it by lofting
#t=t.workplane().rect(length,width).transformed(offset=(-0.25*length,0,tube_h2)).rect(hole_d+2*wall,hole_d+2*wall).loft()
#t=t.faces(">Z").workplane().rect(length,width).workplane()
#t=t.faces(">Z").workplane().spline(pts).close().workplane()
t=t.transformed(offset=(-0.25*length,0,0),rotate=(0,-30,0)).workplane(offset=ofs+tube_h).transformed(rotate=(0,0,180))
t=t.circle(0.5*hole_d+wall).loft()
t=t.faces(">Z").circle(0.5*hole_d).cutBlind(-tube_h)
#t=t.edges(cq.selectors.DirectionSelector(cq.Vector((0,0,1)),tolerance=1)).fillet(corner_r)
#t=t.edges().fillet(corner_r)

#t=t.extrude(tube_h-tube_h2)


# s=s1.extrude(tube_h,False)
# s=s.faces(">Z").shell(-wall)
# t=t.faces(">Z").workplane().rect(length,width).workplane().transformed(offset=(-0.25*length,0,0),rotate=(0,-30,0)).workplane(offset=ofs).circle(0.5*hole_d+wall).loft()
#t=t.edges("|Z").fillet(corner_r)
# t=t.edges("#Z").chamfer(chamfer_r)
# t=t.union(s)
show_object(t)
#show_object(s)


with open('noha.stl','w') as f:
    f.write(cq.exporters.toString(t,'STL',0.01))
f.close()
