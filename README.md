# JK3dPrints

This is a repository of the sources of models I created for 3d printing on 
a [FFF/FDM](https://en.wikipedia.org/wiki/Fused_filament_fabrication) printer,
such as [Prusa i3 MK3S](https://www.prusa3d.com/original-prusa-i3-mk3/). 
Instead of using traditional CADs, I program my models directly in
[OpenSCAD](https://www.openscad.org/), 
in [SolidPython](https://github.com/SolidCode/SolidPython), or in
[Cadquery](https://github.com/CadQuery/cadquery).

- RC [Airboat](https://gitlab.com/jkprague/jk3dprints/-/tree/master/airboat)

- Dishwasher tablet
[basket](https://gitlab.com/jkprague/jk3dprints/-/tree/master/dishwasher_tablet_basket) *(work in progress)*



*Jan Kybic, kybic@fel.cvut.cz*


