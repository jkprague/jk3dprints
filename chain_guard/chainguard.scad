//////////////////////////////////////////////////////////////////////////////////////
///
///  Chainguard for Bicycle - fully parametrized
///
///
///  When I had to replace the crank set of my daughter's bicycle with a Shimano
///  FC-M591 44/32/22, I realized too too late that the big chain ring it came
///  with did not have mounting holes for the chainguard.  So I tried to find
///  a commercial chainguard which I could somehow fit via the 4 mounting screws
///  of the chain ring - but could not find anything reasonable.  Thus, I finally
///  ended up designing and printing such a chainguards myself...
///
///  The chainguard consists of a thinner outer ring, a thicker inner ring and
///  inner arms connection the chainguard to the chain ring mounting screws, which
///  are usually located on a circle with 104mm diameter.  These chain ring 
///  mounting screws consist of an outer barrel with an inside thread and a
///  hollow inner screw which has a 5mm Allen hex key cavity on the outer side
///  and a 4mm hole going through all the way.  While it would be possible to
///  attach the chainguard directly with these screw assemblies, I don't think
///  that this would be reasonable, since these screws bear a lot of force and
///  need to be fastened very tightly - no good idea to squeeze any plastic
///  in between...  
///
///  So we need to somehow use the hole inside these mounting screws to attach
///  the chainguard.  The easiest way (but not so elegant) is to use countersunk
///  M4x20 screws from the inside and use an M4 nut as counterpart on the
///  outside of the chainguard.  A more elegant solution is to cut an inner 
///  M5 thread into the hole the screws, so that we can the just use M5x16
///  Allen key bolts from the outside to mount the chainguard.  For this you
///  need to slightly enlarge the holes using a 4.2mm drill bit and then using
///  an M5 tap cut the inner threads.
///
///  The SCAD file is fully parametrized.  The main parameters are
///   - the number of teeth of the big chain ring (T)
///   - the number of mounting screws (H)
///   - the radius of the circle on which the mounting screws are located (R)
///
///  Auto-Make modules are provided for some of the standard configurations,
///  using 42/44/48 teeth and 4/5 mounting screws.  Note that the 44 teeth
///  chainguard will just fit in the standard 200x200mm printing region,
///  while the 48 teeth version need an extended printing region of at least
///  205x200mm.
///
//////////////////////////////////////////////////////////////////////////////////////
///
///  2015-08-29 Heinz Spiess, Switzerland
///
///  released under Creative Commons - Attribution - Share Alike licence (CC BY-SA)
/// Modified by Jan Kybic for Author Mission
///
//////////////////////////////////////////////////////////////////////////////////////

eh=0.25;  // extrusion height
ew=0.56;  // extrusion width

// build a cube with chamfered edges
module chamfered_cube(size,d=1){
   hull(){
     translate([d,d,0])cube(size-2*[d,d,0]);
     translate([0,d,d])cube(size-2*[0,d,d]);
     translate([d,0,d])cube(size-2*[d,0,d]);
   }
}


// build a cylinder with chamfered edges
module chamfered_cylinder(r=0,r1=0,r2=0,h=0,d=1){
   hull(){
      translate([0,0,d])cylinder(r1=(r?r:r1),r2=(r>0?r:r2),h=h-2*d);
      cylinder(r1=(r?r:r1)-d,r2=(r>0?r:r2)-d,h=h);
   }
}

//function angles(i,H) = i*360/H ;
//function angles(i,H) = 
//  (i==0) ? i*360/H-8.3 :
//    ((i==1) ? i*360/H-4.4 
//            :  i*360/H );

// The 4 mounting pins are not regularly spaced, they are on a rectangle with sides
// 120 and 110mm
alpha=47.490 ; // atan(120/110)*180/p

function angles(i,H) =
  (i==0) ? 0 :
  ((i==1) ? 2*alpha :
   ((i==2) ? 180 :
     180+2*alpha)) ;


module chainguard(
T = 44,    // number of teeth in big chain ring
H = 5,     // number of screw holes

R = 104/2,// inner screw center radius
di = 13,   // inner screw disk diameter
hi = 13,   // height at screw holes

w1 = 7,    // width of thin outer big ring
h1 = 2.5,  // wall height of outer ring
w2 = 5,    // width of thick inner big ring 
h2 = 7.5,  // wall height of outer ring
e2 = 1,    // extra radius correction

ho = 4,    // outer screw hole diameter
do = 10,   // outer screw disk diameter

cac = 0,   // crank angle correction in degrees
cr = 12.5,    // crank pin inset
cw = 41,   // crank width at r1

m5 = 5,    // inner screw hole diameter
m5hh = 7,  // height of M5 screw head
m5hw = 9.2,// height of M5 screw head
m4 = 4,    // diameter of M4 screw
m4n = 6.6, // diameter of M4 nut
useM4 = false,// if true, make M4 nut traps instead of M5 screw head cavities

fn = 120   // circle segments for big circles
){

   r2 = 25.4/4/sin(180/T)+e2; 
   r1 = r2 + w1;
   r3 = r2 - w2;
   ca = 180/H + cac;
   echo("r1,r2,r3:",r1,r2,r3);
   difference(){
      union(){
         difference(){
	    union(){
	       intersection(){
	          // thin outer ring
                  chamfered_cylinder(r=r1,h=h1,d=0.5,$fn=fn);
                 // rotate(ca)translate([-r1,-r1,0])chamfered_cube([r1+r2,2*r1,h1],d=0.5);
	       }
	       // thick inner ring
               chamfered_cylinder(r=r2,h=h2,d=1,$fn=fn);
            }
	    // big hole in rings
            translate([0,0,-1])cylinder(r1=r3,r2=r3-h2/2,h=h2+2,$fn=fn);
         }
	 // mounting arms
	 intersection(){
//            for(i=[0:H-1])rotate(i*360/H)
            for(i=[0:H-1])rotate(angles(i,H))
	       // main arms
	       hull(){
	          // screw end
                  if(r3-di>R){
                     translate([R,0,0])cylinder(r1=di/2+2.5,r2=di/2,h=hi,$fn=32);
		     // ring end
                     translate([r3-di/4,0,0])cylinder(r1=di+2,r2=di/2-2,h=h1,$fn=32);
		     // touch area
		     translate([r3-di,0,0])cylinder(r1=di/4,r2=di/4,h=hi,$fn=32);
                  }else{
                     //translate([R,0,0])cylinder(r1=di+2.5,r2=di,h=h2,$fn=32);
                    translate([R,0,0])cylinder(r1=di,r2=di,h=h2,$fn=32);
                    translate([R,0,h2-0.1])cylinder(r1=di,r2=di/2,h=hi-h2+0.1,$fn=32);
                  }
	       }
            // cut at thick inner ring
            translate([0,0,-1])cylinder(r=r2,h=hi+2,$fn=fn);
          }
	       // central spokes
          if(0)for(i=[0:H-1])rotate(i*360/H)
	       hull(){
		  // ring end
                  translate([r3-di/4,0,0])cylinder(r1=di/4,r2=di/4,h=hi,$fn=32);
	          // screw end
                  translate([R,0,0])cylinder(r1=di/4,r2=di/4,h=hi,$fn=32);
	       }
      }
      // screw holes
      for(i=[0:H-1])rotate(angles(i,H)){
         // main screw hole
         translate([R,0,m5hh+eh+1])cylinder(r=(useM4?m4:m5)/2,h=hi+1,$fn=12);
	 // cavity for screw head
	 if(useM4)
            translate([R,0,-1])cylinder(r1=m4n/cos(30)/2+0.5,r2=m4n/cos(30)/2,h=m5hh+1,$fn=6);
	 else
            translate([R,0,-1])cylinder(r=m5hw/2,h=m5hh+1,$fn=12);
	 // EDITED: no deepening arount screw hole
     //    translate([R,0,hi-1.5])cylinder(r1=di/2+1,r2=di/4,h=hi,$fn=32);
      }
      // EDITED: cavity for crank arm off
      // cavity for crank arm
      //#hull()rotate(ca){
      //   translate([r2-cr,0,-1])cylinder(r=cw/2+1,h=h1+1.5,$fn=52);
      //   translate([r1,0,-1])cylinder(r=cw/2+1,h=h1+1.5,$fn=52);
         // //translate([r1-30,0,-1])cylinder(r=cw/2,h=h1+1.5,$fn=60);
      //}
      // chamfer edges
      rotate(ca){translate([r1+10,0,-1])cylinder(r=cw/2+8,h=h1+1.5,$fn=12);}
      // cavity for chain retainer pin
      rotate(ca-8)translate([r2-cr-di/2,0,-1])cylinder(r=di+0.5,h=hi+1.5,$fn=52);
   }
   // EDITED: support off
   // support for crank arm bridge
   //color("red")for(a=[-8,-3,3,8]){
   //    rotate(ca+a)translate([r3-2,-1,0])cube([r2-r3+4,1,h1-eh]);
   //}
}

module ChainGuard_104mm_T48H4(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,w1=5,e2=0,R=104/2,T=48);
}

module ChainGuard_104mm_T48H4nt(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,w1=5,e2=0,R=104/2,T=48,useM4=true);
}

module ChainGuard_104mm_T44H4(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,R=104/2,T=44);
}

module ChainGuard_104mm_T44H4nt(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,R=104/2,T=44,useM4=true);
}

module ChainGuard_104mm_T42H4(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,R=104/2,T=42);
}

module ChainGuard_104mm_T42H4nt(){ // AUTO_MAKE_STL
    rotate(-45)chainguard(H=4,R=104/2,T=42,useM4=true);
}

module ChainGuard_104mm_T48H5(){ // AUTO_MAKE_STL
    rotate(-180/5)chainguard(H=5,w1=5,e2=0,R=104/2,T=48);
}

module ChainGuard_104mm_T48H5nt(){ // AUTO_MAKE_STL
    rotate(-180/5)chainguard(H=5,w1=5,e2=0,R=104/2,T=48,useM4=true);
}

module ChainGuard_104mm_T44H5(){ // AUTO_MAKE_STL
    rotate(-180/5)chainguard(H=5,R=104/2,T=44);
}

module ChainGuard_104mm_T44H5nt(){ // AUTO_MAKE_STL
    rotate(-180/5)chainguard(H=5,R=104/2,T=44,useM4=true);
}

module ChainGuard_155mm_T44H4(){ // AUTO_MAKE_STL
    rotate(-45+3.2)chainguard(H=4,cac=-3.2,R=155./2,hi=12,di=8,T=44);
}

// ChainGuard_104mm_T44H4();

//chainguard(H=5,R=72.3,T=42,di=10,hi=10,w1=15,e2=1,w2=10,cw=28,cr=12,m5=3.3,m5hh=-1,useM4=false);


// zvysit vysku o 6mm
//chainguard(H=5,R=71.3,T=39,di=9,hi=16,w1=20,e2=1,w2=10,cw=28,cr=12,m5=3.3,m5hh=-1,useM4=false);
// Tohle jsme vytiskli pro Author Linea
//chainguard(H=5,R=72.3,T=39,di=9,hi=16,w1=20,e2=1,w2=10,cw=28,cr=12,m5=3.3,m5hh=-1,useM4=false);

// co dale zmenit
// otvor na kliku posunout doleva (ccw), spojku udelat na vnejsim okraji pod klikou, cca 10mm, nikoliv na vnitrnim
// jeden otvor na sroubek uplne nesedi, ale neni jasne, jestli to neni deformaci od kliky

// kryt pro Author Mission
chainguard(H=4,R=81.394, // sqrt(120**2+110**2)/2
T=48,di=10,hi=13,w1=6.5,h1 = 2.5,  // wall height of outer ring
w2 = 2,    // width of thick inner big ring 
h2 = 8.5,  // wall height of outer ring
e2 = 1,    // extra radius correction
ho = 0,    // outer screw hole diameter
do = 0,   // outer screw disk diameter
cac = 0,   // crank angle correction in degrees
cr = 22.5,    // no crank pin inset
cw = -2,   // crank width at r1
m5 = 2,    // inner screw hole diameter
m5hh = 5,  // height of M5 screw head
m5hw = 0,// height of M5 screw head
m4 = 1,    // diameter of M4 screw
m4n = 0, // diameter of M4 nut
useM4=false);