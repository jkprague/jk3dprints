# Depth meter holder
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

# top rectanglee
bwidth=46
bheight=12
thickness=4
hole_distance=34
hole_d=6.2
hole2_d=4.2
length=110 # distance between the mounting holes and the axis
width=13
head=17
headh=15
head2=6.5 # half width at the top
corner_r=0.5

t0=cq.Workplane("XY")
t=t0.rect(bwidth,bheight)
t=t.extrude(thickness)
# make holes
d=0.5*hole_distance
pts=[(d,0),(-d,0)]
t=t.faces(">Z").workplane().pushPoints(pts).hole(0.5*hole_d)
# vertical bit
s=t0.center(0,0.5*(length+0.5*head)).rect(width,length+0.5*head).extrude(thickness)
t=t.union(s)
# head/attachement
t=t.center(0.5*width,length).transformed(rotate=(0,-90,-90))
t=t.moveTo(-0.5*head,0).lineTo(-head2,headh).radiusArc((head2,headh),head2).lineTo(0.5*head,0).close().extrude(width)
t=t.center(0,headh).circle(0.5*hole2_d).cutThruAll()
t=t.edges().chamfer(corner_r)
#t=t.edges("|Z").fillet(corner_r)
show_object(t)

with open('drzak_hloubkomeru.stl','w') as f:
    f.write(cq.exporters.toString(t,'STL',0.01))
f.close()
