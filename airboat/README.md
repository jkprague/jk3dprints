# RC Airboat for water, ice and snow

![](images/img20200125-175311s.jpg)

Source codes for a 3D printed radio controlled airboat. 
It is very fast and challenging to control on ice and packed snow, with limited
directional stability. Expect a lot of fun and excitement. It is fast enough and
more stable on water. My version does not glide well on grass.

Also on [Thingiverse](https://www.thingiverse.com/thing:4138269).

*Jan Kybic, kybic@fel.cvut.cz*

## License

This is my first attempt at creating a 3d printed airboat. 
I am providing it under the 
[CC BY-NC licence](https://creativecommons.org/licenses/by-nc/4.0/) 
- it is free for you to use 
non-commercially, just give an acknowledgement.


## Description

It has a push configuration propeller and twin air rudders controled by a servo.
It is 200mm wide and 200mm long, the hull is designed to be printed in two main
pieces, each about 200mm times 200mm big. The printed hull weights about 650g,
the complete airboat ready to run including battery about 950g. It might be
possible to make it lighter at the expense of some rigidity.

## Equipment

You will need your own receiver, battery, ESC,
motor, push propeller (diameter around 135mm) and a micro servo (such as [this](https://hobbyking.com/en_us/hxt900-micro-servo-1-6kg-0-12sec-9g.html). 
I salvaged my electronics from a broken Hobby King [Floater
Jet](https://hobbyking.com/en_us/h-king-axn-floater-1280mm-pnf.html) plane,
including a 3S (12V) Lipoly battery. An ESC capable of reversing is useful but
not necessary. The mounting pod for the motor has four holes regularly spaced on
a circle with diameter 23.5mm. 

## Compiling

The model is programmed designed in 
[SolidPython] (https://github.com/SolidCode/SolidPython). Running the
*airboat.py* produces several files: *(airboat.scad, cover.scad,  motor_beam.scad,
rear.scad, rudder_left.scad, connector_pin.scad,  front.scad, rudder_beam.scad,
rudder_right.scad)* which you compile using [OpenSCAD] (https://www.openscad.org/)
to produce '.stl' files, which you in turn run through your favorite slicer
program as usual. The front part of the hull is printed vertically with 1% infill, 3
perimeters and 4 top and bottom layers. The rear part is printed horizontally
with 5% infill. In critical areas (under the connector pin holes, the servo, and
the cable holes), the infill is manually increased to 15% to support bridging.
The cover lid is printed with 15% infill and the remaining pieces (connectors,
rudders and beams) with 5 perimeters and 40% infill. 
I printed the two pieces of the hull, the two rudders, and the four connecting
pins in PETG. The two bars holding the motor and the rudders were printed in ABS
but PETG would be probably also fine.  The electronic compartment cover lid is
also printed in PETG - I originally wanted to print it in FLEX but failed so far.
To print everything takes about 22 hours with a 0.6mm nozzle and 0.35mm layers. 


## Building

The two hull pieces are connected with the 4 connector pins and glued together
by epoxy. I spray painted the hull by a plastic primer and several layers of red
acrylic paint. The servo and the two beams are inserted and motor is mounted. Paper
clip wire is used as the top shaft for the rudders and as the link between the
servos and the rudder horns. When the servo is adjusted, attach it by a screw
and seal with hot glue or silicone. The motor cables are attached to the beam by cable
ties. When the motor and servo cables are thread through the holes, the holes
can be sealed by a hot glue or silicone. I mount the receiver, ESC and battery
by a Velcro tape attached by a hot glue. It is better to mount the electronics
on the walls in case some water gets through.






