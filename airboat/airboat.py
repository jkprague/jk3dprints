#!/usr/bin/python3
#
# 3D printed airboat
# Jan Kybic, kybic@fel.cvut.cz, January 2020



import sys

from solid import *
from solid.utils  import *
from solid.splines import *
from euclid3 import Point2

SEGMENTS=300 # how many segments for a circle
eps=0.001   # very small distance, just to allow singular cases

draw_connectors=False
# all measurements are in mm

# The connector pins to connect the front and rear body parts

connector_length=40
connector_width=4       # with 0.6nozzle and tol=0.2 hole printed as 3.6, pin printed as 3.8x3.9
connector_width_tol=0.4 # how much wider should the hole be (was 0.2) - was not enough (beam from ASA, body PETG, I had to file it)
connector_length_tol=1.5 # how much longer should the hole be
connector_side_sep=8 
connector_top_sep=8 
connector_bottom_sep=connector_top_sep

def connector_pin():
  """ a square connector pin, oriented along 'x' and centered at origin """
  return cube([connector_length,connector_width,connector_width],center=True)

def connector_hole():
  """ a hole for the connector pin, to be subtracted from a solid """
  return cube([connector_length+connector_length_tol,connector_width+connector_width_tol,
                 connector_width+connector_width_tol],center=True)



# ---------- the front part  ------------

front_corner_radius=40 # front corner radius seen from  above
front_radius=2         # front (bow) radius seen from the side
front_length=200       # total length of the front part
slope_length=100       # length of the sloped part
body_height=40         # height of the body
body_width=200         # width of the body
chamfer_body=2         # chamfer for the top and bottom of the body
middle_radius=150      # NOTE: change with care

# needs about 270g with 1% 3d honeycomb infill and 3 perimeters

#assert(chamfer_body<=front_radius)

def point2d(pos):
  """ a very small squaree at position 'pos' """
  return translate(pos)(square(eps,center=True))

def side_profile():
  """ side profile of the front part, in the xy plane. I am doing it with a convex hull of circles. Another approach would be to use splines """
  return hull()(
    translate((-front_length+front_radius,body_height-front_radius))(circle(r=front_radius)), # bow
    translate((-front_length+slope_length,middle_radius))(circle(r=middle_radius))  # bottom middle arc  
    -translate((-front_length+slope_length,middle_radius+body_height-4*front_radius-eps))(
      square(2*middle_radius,center=True)), 
    point2d((0,0)),point2d((0,body_height))   # joint between 
    )

def side_profile_chamfered():
  """ like side_profile but shifted inwards except at the right side, which is only shifted vertically,
  as we do not want any chamfer at the joint betweeen the front and rear pieces """
  return hull()(
    translate((-front_length+front_radius+chamfer_body,body_height-front_radius-chamfer_body))(
      circle(r=front_radius)), # bow
    translate((-front_length+slope_length,middle_radius+chamfer_body))(circle(r=middle_radius))  # bottom middle arc  
    -translate((-front_length+slope_length,middle_radius+body_height-4*front_radius-eps))(
      square(2*middle_radius,center=True)), 
    point2d((0,chamfer_body)),point2d((0,body_height-chamfer_body))   # joint between 
    )

connector_positions=[(0,connector_bottom_sep,connector_side_sep),
                         (0,connector_bottom_sep,body_width-connector_side_sep),
                         (0,body_height-connector_bottom_sep,connector_side_sep),
                         (0,body_height-connector_bottom_sep,body_width-connector_side_sep)]


def front():
  # creat the front body from the profiles with chamfering
  p0=linear_extrude(eps)(side_profile_chamfered()) # reduced profile converted to thin 3d
  p1=linear_extrude(eps)(side_profile()) # profile converted to thin 3d
  p1t=translate((0,0,chamfer_body+eps))(p1)  # p1 translated
  r=(hull()(p0,p1t)
        +translate((0,0,chamfer_body+eps))(linear_extrude(body_width-2*chamfer_body)(side_profile()))
        +hull()(translate((0,0,body_width-chamfer_body-eps))(p1),
                translate((0,0,body_width))(p0)))
  # round the bow corners (seen from above)
  corner1=translate((-front_length+front_corner_radius-2*eps,body_height+eps,front_corner_radius-2*eps))(rotate((90,0,0))(linear_extrude(body_height*2)(arc_inverted(front_corner_radius,180,270))))
  corner2=translate((-front_length+front_corner_radius-2*eps,body_height+eps,body_width-front_corner_radius+2*eps))(rotate((90,0,0))(linear_extrude(body_height*2)(arc_inverted(front_corner_radius,90,180))))
  r-=corner1
  r-=corner2
  # add connector holes
  for pos in connector_positions:
    r-=translate(pos)(connector_hole())
    if draw_connectors:
      r+=color(Magenta)(translate(pos)(connector_pin()))
  return r

# ---------- the rear part ------------

rear_length=200


beam_height=4 # thickness of the beam holding the rudders
beam_width=10

motor_radius=27/2
propeller_radius=135/2
propeller_height=10
motor_length=64-propeller_height # this is the screw, the propeller itself is behind
propeller_rear_gap=10 # how much space behind the propeller
propeller_bottom_gap=10 # how much space under the propeller
motor_gap_length=motor_length+propeller_height+propeller_rear_gap
rudder_length=40 # horizontal rudder length
rudder_height_tol=1 # we make the frame that much higher
rear_rudder_gap=chamfer_body 
motor_xpos=rear_length-motor_gap_length-rudder_length-rear_rudder_gap # motor base position
rudder_shaft_r=2 # rudder shaft radius
rudder_tip_r=1   # rudder tip radius (width is twice that)
rudder_bottom_gap=3 # gap between the rudder and the body
rudder_dip=5 # how much is the rudder inserted inside the body
rudder_dip_rad_tol=0.75 # how much larger should the dip for the rudder be (this is too much, extremely loose)
rudder_top_gap=3 # gap between the rudder and the rudder_beam
rudder_height=2*propeller_radius-rudder_bottom_gap-rudder_top_gap-beam_height # TODO: factor in propeller height
rudder_xpos=rear_length-rudder_length-rear_rudder_gap
rudder_hole_r=0.5 # radius of the hole inside the rudder
rudder_hole_depth=10 
rudder_hole_tol=0.3 # how much bigger should the hole in the beam and body be
inter_rudder_width=50
rudder_left_zpos=0.5*body_width+0.5*inter_rudder_width
rudder_right_zpos=0.5*body_width-0.5*inter_rudder_width
servo_xoffset=5 # servo axis 5mm behind the rudder axis for a 15mm servo arm

# when slicing, I had to add a lot of internal supports manualy, resulting in a bad print quality
# it would be probably better to print the cover separately and explicitely model the empty space

def motor():
  """ motor+propeller along positive z axis, centered at origin """
  return color(Yellow)(cylinder(r=motor_radius,h=motor_length)+
             translate((0,0,motor_length))(cylinder(r=propeller_radius,h=propeller_height)))               

# rudder horn
horn_thickness=2
horn_hole=0.7    # we aim for a 1mm wire but the printer makes the hole smaller
horn_arm=20      # distance from the shaft
horn_offset=5    # distance from the rudder central plane
horn_radius=2.5  # tip radius
horn_base=6      # width at the central plane
horn_height=9    # height above the body

def rudder_horn():
  """ rudder horn to attach the servo links """
  r=hull()(cube((horn_base,horn_thickness,eps),center=True),
             translate((0,0,horn_offset))(
               rotate((-90,0,0))(cylinder(r=horn_radius,h=horn_thickness,center=True))))
  r-=translate((0,0,horn_offset))(
               rotate((-90,0,0))(cylinder(r=horn_hole,h=horn_thickness+2*eps,center=True)))
  return mirror((0,0,1))(r)

def rudder_left():
  """ rudder along y direction, base at (0,0,0) """
  front_shaft=cylinder(r=rudder_shaft_r,h=rudder_height)
  rear_shaft=translate((rudder_length-rudder_tip_r,0,0))(cylinder(r=rudder_tip_r,h=rudder_height))
  r=translate((0,0,rudder_bottom_gap+rudder_dip))(hull()(front_shaft,rear_shaft))
  r+=cylinder(r=rudder_shaft_r,h=rudder_height+rudder_bottom_gap+rudder_dip+rudder_top_gap)
  r-=down(eps)(cylinder(r=rudder_hole_r,h=rudder_hole_depth))
  r-=up(rudder_height+rudder_bottom_gap+rudder_dip+rudder_top_gap-rudder_hole_depth+eps)(cylinder(r=rudder_hole_r,h=rudder_hole_depth))
  r=rotate((-90,0,0))(r)
  r+=translate((horn_arm,rudder_dip+horn_height,0))(rudder_horn())  
  #r+=translate((horn_arm2,rudder_dip+horn_height,0))(rudder_horn())  
  return r
# TODO: rudder horn


def rudder_right():
  return mirror((0,0,1))(rudder_left())

mcad=import_scad("/home/kybic/builds/openscad/libraries/MCAD")
chamfers=import_scad("/home/kybic/builds/Chamfers-for-OpenSCAD")
roundedCube=import_scad("roundedCube.scad").roundedCube

def servo():
  """ servo, oriented with the axis along 'y' through the origin, with y=0 the rear part surface,
patterned after the Hobbyking blue servo from Floater """
  s=mcad.servos.alignds420((0,-22,0),(-90,-90,0),screws=1,axle_lenght=15)
  return s

def servo_hole():
  """ servo_hole, oriented with the axis along 'y' through the origin, with y=0 the rear part surface """
  #s=mcad.servos.alignds420((0,-22,0),(-90,-90,0),screws=1,axle_lenght=15)
  s=translate((-12,-22,-0.5*13))(cube((28.5,22,13)))
  s+=translate((-12,-7.5,-0.5*13))(cube((33.5,7.5,13)))               
  s+=translate((19,-7.5-6+eps,0))(rotate((-90,0,0))(cylinder(r=1,h=6))) # screw
  # shaft for the wire and connector
  shaft_xlength=servo_xoffset+rudder_xpos-motor_xpos
  s+=translate((-shaft_xlength+eps,-22,-0.5*13))(cube((shaft_xlength,13,13)))
  return s

beam_slot_depth=30  # depth of the slots holding the beams
beam_slot_gap=1     # an extra depth to allow for imperfect printing
beam_lip=8     # height of an extra  "lip" to put a pin through in case the beam does not hold
beam_tol=0.4        # how much bigger do we need to make the hole - with 0.6mm nozzle perfect fit
beam_lip_thickness=2 
lip_radius=2
beam_side_offset=chamfer_body+beam_lip_thickness+0.5*(beam_height+beam_tol) # make the beam start at the edge

def maybe_union(x,y): # union of two objects, the first may be None
    return y if x is None else x+y

def maybe_difference(x,y): # set difference x-y, or None if x is None
    return None if x is None else x-y


class BeamSlot:
  """ create a slot for the beam """
  def __init__(self,pos):
    self.pos=pos # an (x,y,z) position of the center of the slot
  
  def make(self,r=None):
    """ given an object 'r', add and subtract what is needed to create the slot """
    if r is not None:
      r-=translate(self.pos)(
        translate((-0.5*beam_width,5*eps-(beam_slot_depth+beam_slot_gap),-0.5*(beam_height+beam_tol)))(
        cube((beam_width,beam_slot_depth+beam_slot_gap,beam_height+beam_tol))))
    #lip=hull()(
    #  translate((-0.5*beam_width-beam_lip,0,(beam_height+beam_tol)*0.5))(
    #               cube((beam_width+2*beam_lip,beam_support,lip_thickness))),
    #  translate((-0.5*beam_width,beam_support+beam_lip,(beam_height+beam_tol)*0.5))(
    #               cube((beam_width,eps,lip_thickness))))
    #lip=translate((-0.5*beam_width,0,(beam_height+beam_tol)*0.5))(cube((beam_width,beam_lip,lip_thickness)))
    #lip=translate((-0.5*beam_width,0,(beam_height+beam_tol)*0.5))(
    #  chamfers.Chamfer.chamferCube((beam_width,beam_lip,lip_thickness),
    #    [[0,1,1,0],[0,0,0,0],[0,0,0,0]],lip_chamfer))
    lip=translate((-0.5*beam_width,-eps,(beam_height+beam_tol)*0.5))(
      roundedCube((beam_width,beam_lip,beam_lip_thickness),lip_radius,zcorners=[True,True,False,False]))
    lipt=translate(self.pos)(lip+mirror((0,0,1))(lip)) # add the mirrored version
    r=maybe_union(r,lipt)
    return r

compartment_gap_sides=20 # gap around the compartment sides
compartment_gap_front=30 # gap around the compartment in front and in the back
compartment_gap_rear=50 
compartment_radius=10
compartment_width=body_width-2*(beam_side_offset)-2*beam_lip_thickness-2*beam_height-2*compartment_gap_sides
compartment_length=rudder_xpos-compartment_gap_front-compartment_gap_rear
compartment_depth=body_height-5 # almost all the way to the bottom
compartment_rim=10
compartment_rim_thickness=2
compartment_lip_width=1.5 # a lip around the lid for good closing
compartment_lip_height=2

print("compartment: ", compartment_width, compartment_length)

def rounded_rectangle(x,y,r):
  """ rounded rectangle """
  c=circle(r=r)
  x2=x/2 
  y2=y/2
  return hull()(
    translate((-x2,-y2))(c),
    translate((-x2,y2))(c),
    translate((x2,y2))(c),
    translate((x2,-y2))(c))
 
def chain_hull_union(s):
  """ union of pairwise convex hulls, useful for profiles """
  r=None
  for i in range(1,len(s)):
    r=maybe_union(r,hull()(s[i-1],s[i]))
  return r

def profile(z,p): # profile p in plane z above the surface
  return translate((0,0,z))(linear_extrude(eps)(p))


class Compartment:
  """ a compartment for the battery, receiver etc. It consists of a hole, rim and lid. It is oriented with 'z'
   upwards """
  def __init__(self,pos,rot,sx,sy,sz):
    self.pos=pos # position of the top center (not including the rim)
    self.rot=rot
    self.sx=sx   # size
    self.sy=sy
    self.sz=sz


  def make(self,r=None):
    inner=rounded_rectangle(self.sx,self.sy,compartment_radius) # center square with round corners
    inner_extruded=translate((0,0,-self.sz-4*eps)) (
      linear_extrude(self.sz+compartment_rim+8*eps)(inner)) # this is the hole = compartment itself
    t=compartment_rim_thickness
    outer=offset(t)(inner)
    outer_lip=offset(compartment_lip_width)(outer)
    #outer=rounded_rectangle(self.sx+2*t,self.sy+2*t,compartment_radius+t) # center square with round corners
    outer_extruded=translate((0,0,-self.sz)) (
      linear_extrude(self.sz+compartment_rim)(outer)) 
    # create a lip
    p1=profile(compartment_rim,outer) # top profile
    p2=profile(compartment_rim-compartment_lip_width,outer_lip) # wider part
    p3=profile(compartment_rim-compartment_lip_width-compartment_lip_height,outer_lip)
    p4=profile(compartment_rim-2*compartment_lip_width-compartment_lip_height,outer)
    pospart=union()(outer_extruded,chain_hull_union((p1,p2,p3,p4)))
    r=maybe_union(r,translate(self.pos)(rotate(self.rot)(pospart)))
    r=maybe_difference(r,translate(self.pos)(rotate(self.rot)(inner_extruded)))  # we have added the rim
    #r+=color(Green)(inner_extruded)
    return r

# hole to lead the cable from the motor
cable_hole_width=10
cable_hole_height=4
cable_hole_depth=20
motor_beam_xpos=motor_xpos-0.5*beam_width
cable_hole_xpos=motor_beam_xpos+0.5*beam_width+10 # xposition of the side of the hole

beam_radius=50
beam_rudder_hole_r=1 # radius of the hole for the rudder (paper clip) shaft

rudder_top_block=3 # height of the block covering the rudder shafts (paper clip)
rudder_top_border=1
rudder_top_inside=beam_width-2*rudder_top_border # rectangular hole inside

def rudder_beam():
  z1=beam_side_offset
  z2=body_width-beam_side_offset
  z3=z1+beam_radius-0.5*beam_height
  z4=z2-beam_radius+0.5*beam_height
  y1=body_height-beam_slot_depth
  y2=body_height+rudder_height_tol+rudder_bottom_gap+rudder_top_gap+rudder_height # height of the beam center
  x=rudder_xpos
  y3=y2-beam_radius+beam_height
  r=translate((x-0.5*beam_width,y1,z1-0.5*beam_height))(cube((beam_width,y3-y1,beam_height)))
  r+=translate((x-0.5*beam_width,y1,z2-0.5*beam_height))(cube((beam_width,y3-y1,beam_height)))
  r+=translate((x-0.5*beam_width,y2,z3-eps))(cube((beam_width,beam_height,z4-z3+2*eps)))
  #pts=[ (x,y1,z1),(x,y3,z1),(x,y2-10,z1+10),(x,y2,z3),(x,y2,z4),(x,y2-10,z2-10),(x,y3,z2),(x,y1,z2) ]
  # rounded parts of the beams
  corner=rotate((0,-90,0))(linear_extrude(beam_width)(
    arc(beam_radius,0,90,segments=SEGMENTS)-arc(beam_radius-beam_height,0,90,segments=SEGMENTS)))
  r+=translate((x+0.5*beam_width,y3,z4))(corner)
  r+=translate((x+0.5*beam_width,y3,z3))(mirror((0,0,1))(corner))
  # covers over holes
  block=(cube((beam_width,rudder_top_block,beam_width))-
           translate((rudder_top_border,-eps,rudder_top_border))(
             cube((rudder_top_inside,rudder_top_block+2*eps,rudder_top_inside))))
  r+=translate((x-0.5*beam_width,y2+beam_height-eps,rudder_left_zpos-0.5*beam_width))(block)
  r+=translate((x-0.5*beam_width,y2+beam_height-eps,rudder_right_zpos-0.5*beam_width))(block)
  # holes in the beams
  hole=rotate((-90,0,0))(cylinder(r=rudder_hole_r,h=beam_height+5*eps))
  r-=translate((x,y2-eps,rudder_left_zpos))(hole)
  r-=translate((x,y2-eps,rudder_right_zpos))(hole)
  return r

motor_mount_thickness=3.6
motor_hole_r=23.5/2  # distance from the motor mount hole to the center 
motor_mount_r=motor_radius+2
motor_beam_shift=0.5*motor_hole_r # vertical shift of the motor_beam wrt motor axis

leg_support=5 # this is the height of the square part of the motor beam support
leg_xsize=20 # size of the "leg" to prevent the motor beam support from leaning forward
leg_ysize=leg_xsize

def motor_mount():
  r=cylinder(r=motor_mount_r,h=motor_mount_thickness)
  #r-=up(motor_mount_thickness)(cylinder(r=motor_radius,h=motor_dip+eps))
  hole=translate((motor_hole_r,0,-eps))(
           union()(mcad.nuts_and_bolts.nutHole(2,tolerance=0.2), # M2 screw and hole
                   mcad.nuts_and_bolts.boltHole(2,motor_mount_thickness+2*eps,tolerance=0.2)))
  for a in [0,90,180,270]:
    r-=rotate((0,0,a))(hole)
  r=rotate((0,90,0))(r)
  return translate((-motor_mount_thickness,0,0))(r)


def motor_beam():
  motor_y=body_height+propeller_bottom_gap+propeller_radius # y-position of the motor axis
  x=motor_xpos-0.5*beam_width
  r=translate((motor_xpos,motor_y,0.5*body_width))(motor_mount())
  z1=beam_side_offset
  z2=body_width-beam_side_offset
  z3=z1+beam_radius-0.5*beam_height
  z4=z2-beam_radius+0.5*beam_height
  y1=body_height-beam_slot_depth
  y2=motor_y+motor_beam_shift-0.5*beam_height # height of the upper beam bottom
  y4=motor_y-motor_beam_shift-0.5*beam_height # height of the lower beam bottom
  y3=y2-beam_radius+beam_height
  r+=translate((x-0.5*beam_width,y1,z1-0.5*beam_height))(cube((beam_width,y3-y1,beam_height))) #vertical
  r+=translate((x-0.5*beam_width,y1,z2-0.5*beam_height))(cube((beam_width,y3-y1,beam_height)))
  r+=translate((x-0.5*beam_width,y2,z3-eps))(cube((beam_width,beam_height,z4-z3+2*eps))) # upper beam
  corner=rotate((0,-90,0))(linear_extrude(beam_width)(
    arc(beam_radius,0,90,segments=SEGMENTS)-arc(beam_radius-beam_height,0,90,segments=SEGMENTS)))
  r+=translate((x+0.5*beam_width,y3,z4))(corner)
  r+=translate((x+0.5*beam_width,y3,z3))(mirror((0,0,1))(corner))
  r+=translate((x-0.5*beam_width,y4,z1-eps))(cube((beam_width,beam_height,z2-z1+2*eps))) # lower beam
  negcorner=rotate((0,-90,0))(linear_extrude(beam_width+10*eps)(arc_inverted(beam_radius+eps,0,90,segments=SEGMENTS)))
  r-=translate((x+0.5*beam_width+eps,y3,z4))(negcorner)
  r-=translate((x+0.5*beam_width+eps,y3,z3))(mirror((0,0,1))(negcorner))
  # lips
  lip=hull()(
      translate((-0.5*beam_width-leg_xsize,0,-0.5*beam_height))(
                   cube((beam_width+leg_xsize,leg_support,beam_height))),
      translate((-0.5*beam_width,leg_support+leg_ysize,-0.5*beam_height))(
                   cube((beam_width,eps,beam_height))))
  r+=translate((x,body_height,z1))(lip)
  r+=translate((x,body_height,z2))(mirror((0,0,1))(lip)) # add the mirrored version
  return r


def rear():
  rear_side=linear_extrude(eps)(polygon([(0,0),(0,body_height),(rear_length,body_height),(rear_length,0)]))
  rear_side_chamfered=linear_extrude(eps)(polygon([(0,chamfer_body),(0,body_height-chamfer_body),
       (rear_length-chamfer_body,body_height-chamfer_body),(rear_length-chamfer_body,chamfer_body)]))
  r=hull()(rear_side_chamfered,translate((0,0,chamfer_body))(rear_side)) # right side
  r+=hull()(translate((0,0,chamfer_body-eps))(rear_side),                # central part
            translate((0,0,body_width-chamfer_body))(rear_side))
  r+=hull()(translate((0,0,body_width-chamfer_body-eps))(rear_side),                # central part
            translate((0,0,body_width))(rear_side_chamfered))
  for pos in connector_positions:
    r-=translate(pos)(connector_hole())
    if draw_connectors:
      r+=color(Magenta)(translate(pos)(connector_pin()))
  for zp in [rudder_left_zpos,rudder_right_zpos]: # dips for rudders
    r-=translate((rudder_xpos,body_height-rudder_dip+10*eps,zp))(
      rotate((-90,0,0))(
      cylinder(r=rudder_shaft_r+rudder_dip_rad_tol,h=rudder_dip)))
  # slots to insert the beams
  r=BeamSlot((motor_beam_xpos,body_height,beam_side_offset)).make(r)
  r=BeamSlot((motor_beam_xpos,body_height,body_width-beam_side_offset)).make(r)
  r=BeamSlot((rudder_xpos,body_height,beam_side_offset)).make(r)
  r=BeamSlot((rudder_xpos,body_height,body_width-beam_side_offset)).make(r)
  # main compartment
  r=Compartment((compartment_gap_front+compartment_length*0.5,body_height,0.5*body_width),(-90,0,0),compartment_length,compartment_width,compartment_depth).make(r)
  # servo
  r-=(translate((servo_xoffset+rudder_xpos,body_height+10*eps,0.5*body_width))(servo_hole())) 
  # cable hole to the engine
  r-= translate((cable_hole_xpos,body_height-cable_hole_depth+5*eps,
        beam_side_offset-0.5*cable_hole_height))(
          cube((cable_hole_width,cable_hole_depth,cable_hole_height)))
  cut_depth=50
  r-= translate((cable_hole_xpos,body_height-cable_hole_depth+5*eps,
        beam_side_offset-0.5*cable_hole_height))(
          cube((cable_hole_width,cable_hole_width,cable_hole_height+cut_depth)))
  return r  

#### cover of the compartment, to be printed from FLEX

#cover_hor_tol=-1 # by how much should the cover be bigger in the horizontal direction, can be negativee
#cover_hor_tol=1 # by how much should the cover be bigger in the horizontal direction, can be negativee - too little for PET
cover_hor_tol=0.2 # by how much should the cover be bigger in the horizontal direction, on each side
#cover_vert_tol=0.5 # vertical gap between the lips
cover_height=8 # vertical gap between the lips
cover_lip_width=0.1
#cover_thickness=2 # horizontal thickness
cover_thickness=1 # horizontal thickness
#cover_wall=2 # wall thickness

# when printing from PETG/ASA
cover_wall=1 # wall thickness


def cover():
  """ build it at the origin with z=up, z=0 the inner surface of the base """
  # center square with round corners
  inner=rounded_rectangle(compartment_length,compartment_width,compartment_radius) 
  outer=offset(compartment_rim_thickness)(inner)
  outer_lip=offset(compartment_lip_width+cover_hor_tol)(outer)
  cover_lip=offset(-cover_lip_width)(outer_lip)
  outer_wall=offset(cover_wall)(outer_lip)
  # let us build the negative profile from z=0 downwards  
  p=[profile(0,outer_lip)]
  z=cover_height-3*cover_lip_width
  p+=[profile(-z,outer_lip)]
  z+=cover_lip_width
  p+=[profile(-z,cover_lip)]
  z+=cover_lip_width
  p+=[profile(-z,cover_lip)]
  z+=cover_lip_width
  p+=[profile(-z-eps,outer_lip)]
  negpart=chain_hull_union(p)
  base=down(z)(linear_extrude(cover_thickness+z)(outer_wall))
  print("z=",z)
  return up(compartment_rim)(base-negpart)

### now assemble everything

def body():
  """ show the assembled airboat """
  #return BeamSlot((0,0,0)).make()
  #return Compartment((0,0,0),compartment_length,compartment_width,compartment_depth).make()
  r=front()+color(Green)(rear())
  r+=translate((motor_xpos,body_height+propeller_bottom_gap+propeller_radius,0.5*body_width))(rotate((0,90,0))(motor()))
  r+=color(Pine)(translate((rudder_xpos, body_height-rudder_dip,rudder_left_zpos))(rudder_left()))
  r+=color(Pine)(translate((rudder_xpos, body_height-rudder_dip,rudder_right_zpos))(rudder_right()))
  r+=color(Cyan)(translate((servo_xoffset+rudder_xpos,body_height,0.5*body_width))(servo()))
  r+=color(Steel)(rudder_beam())
  r+=color(Steel)(motor_beam())
  r+=color(Black)(translate((compartment_gap_front+compartment_length*0.5,body_height,0.5*body_width))(
    rotate((-90,180,0))(cover())))
  return  rotate((90,0,0))(r)

def render(object,filename):
  file_out = scad_render_to_file(object, file_header=f'$fn = {SEGMENTS};', filepath=filename, include_orig_code=False)
  print(f"{__file__}: SCAD file written to: {file_out}")


if __name__ == '__main__':  
  render(rudder_beam(),"rudder_beam.scad")
  render(motor_beam(),"motor_beam.scad")
  render(connector_pin(),"connector_pin.scad")
  render(front(),"front.scad")
  render(rear(),"rear.scad")
  render(rudder_right(),"rudder_right.scad")
  render(rudder_left(),"rudder_left.scad")
  render(cover(),"cover.scad")
  render(body(),"airboat.scad")



