// I found this at https://libre3d.com/category/702/Music/listings/829/Parametric-Saxophone-Mouthpiece.html
// trying to modify to resemble a beginner's alto mouthpiece Yamaha 4C
// Jan Kybic

//c=25.4; //convert each dimension to mm
//$fn=100;//quality of curve

//ol=89; //overall length
//ma=70;//%major length
//mi=ol-ma;//minor lenth
//ma_dmax=28 ; //maximum diameter

/* ma_dmin=17.5; // diameter at the front end (virtual) */
/* mi_dmax=ma_dmax; // rear side front diameter */
/* mi_dmin=22;      // rear side rear diameter */
/* radius=.22; */
/* ra=7; //reed angle */
/* bcr=ma_dmax;//baffle curve radius (outside) */
/* bcr_angle=18; */
/* bcor=.15*c;//raise baffle to adjuct point linup */
/* bt=.1*c;//thickness of plactic at baffle */
/* chw=.4*c;//front chamber width */
/* chh=.5*c;//front chamber height */
/* ca=2.2;//chamber angle adjust */
/* t=.075*c;//tip distance */
/* to=.25*c;//tip alignment */
/* bore=.626*c;//bore diameter */
/* bp=.4*ma;//%penetration of bore into the major  */

c=25.4; //convert each dimension to mm
$fn=100;//quality of curve

ol=3.73*c; //overall length
ma=.69*ol;//%major length
mi=ol-ma;//minor lenth
ma_dmax=1.192*c;
//ma_dmax=28;
ma_dmin=.688*c;
mi_dmax=1.06*c;
mi_dmin=.8*c;
radius=.22;
ra=5; //reed angle
bcr=ma_dmax;//baffle curve radius (outside)
bcr_angle=20;
bcor=4.3 ; //bcor=.15*c;//raise baffle to adjunct point linup
bt=2.5;//thickness of plactic at baffle
//chw=.4*c;//front chamber width
//chh=.5*c;//front chamber height
chw=10.2 ;
chh=12.7 ;
ca=2.2;//chamber angle adjust
t=.075*c;//tip distance
to=.25*c;//tip alignment
//bore=.626*c;//bore diameter
bore=16 ;//bore diameter
bp=.35*ma;//%penetration of bore into the major
tr=16 ; // tip radius

// tip opening
topen=1.6 ; 
brp=22  ; // distance from the tip to the breaking point
bore2=16 ;
boreofs=10 ;


module body(){
rotate([0,90,0])cylinder(d1=ma_dmax,d2=ma_dmin,h=ma);
rotate([0,-90,0])cylinder(d1=mi_dmax,d2=mi_dmin,h=mi);
}

fa=atan2(topen,brp);

module reed(){ // used for cutting the table
rotate([0,-ra,0]) { 
  translate([ol/2-radius,0,-mi_dmax])
    { cube([ol*1.4,ma_dmax,ma_dmax],center=true);//baffle angle
  rotate([0,-fa,0])
     translate([ol-brp,0,0])cube([ol*2,ma_dmax,ma_dmax],center=true);//facing curve
    }
}
}


module baffleexterior(){
module block(){
translate([0,-bcr,0])cube([ma,bcr*2,bcr*2]);
rotate([0,-bcr_angle,0])translate([0,-bcr,0])cube([ma,bcr*2,bcr*2]);
}
mr=((ma_dmax*.67)+(ma_dmin*.33))/2;
p=sqrt((bcr*bcr)-(mr*mr));

translate([.5*ma,0,bcor])rotate([0,bcr_angle,0])difference(){
block();
translate([-1,0,-p])rotate([0,90,0])cylinder(r=bcr,h=ma*1.1); // changed here translate [0,0,-p]->[-1,0,-p]
}
}


module bore(){
//translate([bp,0,0])rotate([0,-90,0])cylinder(d=bore,h=ol);
translate([bp,0,0])rotate([0,-90,0])
{ translate([0,0,boreofs])   cylinder(d=bore,h=ol);
    cylinder(d=bore2,d2=bore,h=boreofs);
}
}

module chamber(){
rotate([0,bcr_angle-ca,0])translate([ol*.2,-chw/2,bcor*1.6-bt-(chh-chw)])cube([ol,chw,chh]);
}



/* module tip(){ */
/* difference(){ */
/* difference (){ */
/*  sphere(r=ma+t, h=t, center=true); */
/*  //mouthpiece(); */
/* } */
/* translate([0,0,(ma+t)-to+t])cube([2*(ma+t),2*(ma+t),2*(ma+t)],center=true); */
/* translate([0,0,-(ma+t)-to])cube([2*(ma+t),2*(ma+t),2*(ma+t)],center=true); */
/* translate([-t,0,0])cube([2*(ma+t),2*(ma+t),2*(ma+t)],center=true); */

/* } */
/* } */

th=30 ; // height of the auxiliary shape to cut the tip
eps=0.1 ;
tofs=0 ; // tip offset in x
module tip() {
difference() {
 translate([ma-tr+tofs,-tr,-th+eps]) cube([tr,2.1*tr,th]);
 translate([ma-tr+tofs,0,-th]) cylinder(r=tr,h=th+2*eps) ;
}
}

module mouthpiece () {

//tip();
difference(){
difference(){
difference(){
difference(){
hull(){

body();
}
reed();
}
baffleexterior();
}
bore();
}

chamber();
tip() ;
}}




mouthpiece();
//tip();
