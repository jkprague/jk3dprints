# Lavender box
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq
import math

length=80
width=50
height=20
thickness=2

cover_height=10
top_gap=1
corner_radius=10
gap=0.2
chamfer_size=1
lip_chamfer=0.3
margin=5
hole_size=2
hole_sep=hole_size+2 # basic step of the hexagonal lattice
hx=7   # number of holes
hy=7
tooth_length=10 # the tooth/indendation for locking
tooth_height=0.5
tooth_width=2
tooth_offset=2
eps=1e-3

doHoles=True # set to false for speed

lip_height=cover_height - thickness - top_gap
bottom_height=height-cover_height
lip_offset=thickness+gap

# create the bottom part as a solid
b=cq.Workplane("XY").rect(length,width).extrude(bottom_height)
b=b.edges("|Z").fillet(corner_radius)
# lip as a solid
l=b.faces(">Z").workplane().rect(length-2*lip_offset,width-2*lip_offset).extrude(lip_height,False)
l=l.edges("|Z").fillet(corner_radius-lip_offset)
b=b.union(l)
# shell out
b=b.faces(">Z").shell(-thickness)
b=b.faces("<Z").edges().chamfer(chamfer_size)
b=b.faces(">Z").chamfer(lip_chamfer)
#b=(b.faces(">Z").workplane(centerOption="CenterOfMass").transformed(rotate=(0,0,0),offset=(0,0))
#     .text("14.2.2020",8,0.5,cut=False,combine=True,font="Verdana",kind='regular'))

# create the cover
c=cq.Workplane("XY").workplane(offset=height).rect(length,width).extrude(-cover_height)
c=c.edges("|Z").fillet(corner_radius)
c=c.faces("<Z").shell(-thickness)
c=c.faces(">Z").chamfer(chamfer_size)
#c=(c.faces(">X").workplane(centerOption="CenterOfMass").transformed(rotate=(0,0,0),offset=(0,0))
#     .text("LAVANDE",8,0.5,cut=False,combine=True,font="Verdana",kind='regular'))


if doHoles:
    # make the holes in a hexagonal grid
    pts=[]
    c60=0.5 # cos(60deg)
    s60=math.sqrt(0.5) # sin(60)
    for i in range(-hy,hy+1):
      for ix in range(-hx,hx+1-(i%2)):
          x=hole_sep*(ix+(i%2)*c60)
          y=hole_sep*(i*s60)
          pts+=[(x,y)]
    #c=c.faces(">Z").workplane().pushPoints(pts).polygon(6,hole_size).cutThruAll()
    c=c.faces(">Z").workplane().pushPoints(pts).circle(hole_size/2).cutThruAll()

# the tooth
#t=cq.Workplane("XZ").rect(tooth_length,tooth_width).extrude(tooth_height)
t=b.faces("<Y").workplane(offset=-lip_offset).transformed(offset=(0,0.5*bottom_height+lip_height-tooth_offset))
t=t.rect(tooth_length,tooth_width).extrude(tooth_height,False)
t=t.faces("<Y").chamfer(tooth_height-eps)
b=b.union(t).union(t.mirror("XZ"))

# cut the dent to the cover
ct=t.translate((0,-gap,0))
c=c.cut(ct).cut(ct.mirror("XZ"))
show_object(c,name="cover")

show_object(b,name="bottom")
#show_object(ct,name="ct")

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(b,"lvboxbottom.stl")
write_object(c,"lvboxcover.stl")

