# A basket for dishwasher tablets

*This is a work in progress*

[SolidPython](https://github.com/SolidCode/SolidPython) source for a little
basket to be put into a dishwasher. Printed in ABS, solid (100% infill, 4
perimeters, 4 top and bottom layers).
