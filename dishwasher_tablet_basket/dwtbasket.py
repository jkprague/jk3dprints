# Dishwasher tablet basket
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq

length=45
width=32
height=30 # just the basket part

corner_radius=5
wall_thickness=1
eps=0.01
grid_spacing=5
grid_width=1
rim=1
chamfer_size=0.1

# create a uniform rectangular grid in the XY plane 'p' extruded to Z
# spacing is the distance between the grid lines, width is their width, and height is height.
# normally, this grid is made bigger (by default -xrange=200..xrange x -xrange=200..xrange=200)
# and then intersected with the shape you want 
#  
def grid(spacing,width,height=10,xr=50,yr=50):
    # TODO: It is stupid to repeat the code but there seems to be a bug in the "transformed" method
    # it has been fixed by an unmerged patch
    # https://github.com/CadQuery/cadquery/issues/254
    p=cq.Workplane("XY").workplane(offset=1)
    pts=[]
    for i in range(int(-xr/spacing),int(xr/spacing)+1):
      pts+=[(i*spacing,0)]
    p=p.pushPoints(pts).rect(width,yr*2)
    p=p.extrude(height)
    p=p.union(p.rotate((0,0,0),(0,0,1),90))
    #pts=[]
    #for i in range(int(-yr/spacing),int(yr/spacing)+1):
    #  pts+=[(0,i*spacing)]
    #p=p.pushPoints(pts).rect(2*xr,width)
    #p=p.extrude(height)
    return p

def bottom(b,delta): # make the bottom shape smaller by 'delta'
  r=b.faces("<Z").workplane().rect(length-2*delta,width-2*delta)
  r=r.extrude(wall_thickness+eps,False)
  return r.edges("|Z").fillet(corner_radius-wall_thickness)

def side(sel,b,delta): # make the side shape
  f=b.faces(sel).workplane(offset=-eps) # front and back faces
  return f.rect(length-2*corner_radius-delta,height-2*rim-delta).extrude(wall_thickness+2*eps,False)

  

# make the basic shape as a solid
b=cq.Workplane("XY").rect(length,width).extrude(height)
#show_object(b)
# round the vertical edges
b=b.edges("|Z").fillet(corner_radius)
#f2=b.faces("-Y").workplane(invert=True) 
# as there is no offset operation, we need to redo the extrusion and rounding
r1=bottom(b,wall_thickness-eps)
r2=bottom(b,wall_thickness)
fs1=side(">Y",b,wall_thickness+rim-eps)
fs2=side(">Y",b,wall_thickness+rim)
bs1=side("<Y",b,wall_thickness+rim-eps)
bs2=side("<Y",b,wall_thickness+rim)
# shell it out
b=b.faces("+Z").shell(wall_thickness)
# cut out all windows
# for some reason, cutting one by one does not work, so we need to union and cut
#show_object(r1,options={"rgba": (255,0,0,0.0)})
#b=b.cut(r1).cut(fs1).cut(bs1)
b=b.cut(r1.union(fs1).union(fs1).union(bs1))
#show_object(fs1)
# bottom grid
#g=grid(grid_spacing,grid_width,xr=30,yr=30,height=-3)
#g=
#g=cq.Workplane("XY").workplane(offset=10).rect(200,10).extrude(-20)
g=grid(grid_spacing,grid_width,xr=30,yr=30,height=-3)

#g=cq.Workplane("XY").box(100,5,10)
#show_object(g,options={"rgba": (255,0,0,0.0)})
#show_object(r2,options={"rgba": (0,255,0,0.0)})
g=g.intersect(r2) #.edges("#Z").chamfer(chamfer_size)
#show_object(g,options={"rgba": (255,0,0,0.0)})
b=b.union(g)
#show_object(g,options={"rgba": (0,0,255,0.0)})
# side grid
#g=grid(bs2.faces("+Y").workplane(offset=1,centerOption="CenterOfMass"), #.transformed(offset=(0,-1.5,0)),
#   grid_spacing,grid_width,xr=30,yr=30,height=-3)
#show_object(g,options={"rgba": (255,0,0,0.0)})
#g=g.rotateAboutCenter((0,1,0),45)
#g=g.intersect(bs2)
#b=b.union(g)
# side grid
#g=grid(fs2.faces("-Y").workplane(offset=1).transformed(offset=(0,-1.5,0),rotate=(90,0,0)),
#   grid_spacing,grid_width,xr=30,yr=30,height=-3)
#g=g.rotateAboutCenter((1,0,0),90).rotateAboutCenter((0,1,0),45)
#g=g.intersect(fs2)
#b=b.union(g)

#.edges("#Y").chamfer(chamfer_size)


# chamfer top
#b=b.faces("+Z").chamfer(chamfer_size)
# side grids
#for sel in ["+Y"]:
#  b=b.faces(sel).rect(length-2*corner_radius,height-2*rim).cutBlind(wall_thickness+eps)
#f1=f1.edges("|Z").fillet(1)
#show_object(f1)
#b=b.cut(f1)
#show_object(f1)
#for fi in [f1]:
#g=g.cut(r2)
#r2=r2.cut(g)
#b=g.intersect(r2)
#show_object(g,options={"rgba": (255,0,0,0.0)})
#show_object(b,options={"rgba": (0,255,0,0.0)})
# #b=b.union(g)
# #r=r.union(g)
# #r=r.cut(g)
show_object(b)


# make a grid on the bottom face
#show_object(g)
