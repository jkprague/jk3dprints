# Dishwasher tablet basket
# Solidpython script
#
# Jan Kybic, kybic@fel.cvut.cz, 2020

import sys

from solid import *
from solid.utils  import *
from solid.splines import *
from euclid3 import Point2

SEGMENTS=60 # how many segments for a circle
eps=0.001   # very small distance, just to allow singular cases


length=45
width=35
height=30 # just the basket part

corner_radius=5
swall_thickness=1 # side wall thickness
fwall_thickness=2 # front/back meshed wall thickness
bwall_thickness=2 # bottom thickness
eps=0.01
grid_spacing=6
grid_width=2
rim=1 # horizontal rim for the bottom face
extra_topbottom=1 # vertical rim at the top and bottom

def solidbody(deltal,deltaw):
  c=cylinder(r=corner_radius,h=height)
  x=0.5*length-corner_radius-deltal
  y=0.5*width-corner_radius-deltaw
  return hull()(
    translate((-x,-y))(c),
    translate((-x,y))(c),
    translate((x,y))(c),
    translate((x,-y))(c))


def maybe_union(x,y): 
    """ union of two objects, the first may be None """
    return y if x is None else x+y


def _gridx():
   """ set of parallel beams, to be used in `grid` """
   y=None
   ngrid=10
   grid_height=10
   grid_extend=(2*ngrid+1)*grid_spacing
   for i in range(-ngrid,ngrid+1):
     y=maybe_union(y,right(i*grid_spacing)(cube((grid_width,grid_extend,grid_height),center=True)))
   return y                  

def grid():
   """ Make a regular rectangular grid """
   g=_gridx()
   return g+rotate((0,0,90))(g)

#def enlarge(x):
#  return union()(
#    up(eps)(x),down(eps)(x),up(eps)(x),forward(eps)(x),back(eps)(x))

def enlarge(x):
  return minkowski()(sphere(eps),x)



hook_width=3   # width (towards 'x') of the hook material
hook_height=3  # height (towards 'y') of the hook material
hook_opening=6 # size of the opening
hook_tol=1     # how much vertical space we need when inserting the hook
hook_gap=0.2   # horizontal gap around the hook insert, on each side
supports=False # generate also the supports
suppw=0.2 # support width, 0.2 is one layer
hook_lip=10    # vertical dimension of the extrusion to hold the hook in place
hook_width_tol=0.2 # tolerance of the pocket ceiling
hwall_thickness=2

def hook():
  """ right looking hook, with center base at x=0, up is 'y', raised towards positive 'z' """
  w=hook_width
  d=hook_opening
  h=linear_extrude(hook_height)(
    forward(d+w)(circle(d/2+w)-circle(d/2)-translate((-d,-2*d))(square(2*d,d+w))))
  h+=translate((-d/2-w,-hook_lip+eps,0))(cube((w,d+hook_lip+w+eps,hook_height)))
  h+=translate((d/2,d+eps,0))(cube((w,w+eps,hook_height))) # tip of the opening
  #h+=translate((-d/2-3*w-2*hook_gap,-hook_lip-w+eps,0))(cube((3*w+2*hook_gap+eps,w,w))) # bottom horizontal part
  #h+=translate((-d/2-3*w-2*hook_gap,-hook_lip-eps,0))(cube((w,hook_lip+eps,w)))
#  h+=translate((-d/2-2*w-2*hook_gap-eps,-hook_lip-eps))(
#    hull()(
#      cube((w+2*hook_gap+2*eps,eps,w)),
#      translate((0,w,w))(cube((w+2*hook_gap+2*eps,eps,eps)))))
#  w2=0.4142*w # (sqrt(2)-1)
#  d=hook_opening ; d2=0.5*d
#  pts=[(0,0),(d2,w),(d2,d+w),(0,1.5*d+w),(-d2,d+w),(-d2,d),
#       (-d2-w,d),(-d2-w,d+w+w2),(-w2,1.5*d+2*w),(w2,1.5*d+2*w),(d2+w,d+w+w2),(d2+w,0)]
#  h=linear_extrude(hook_height)(polygon(pts))
#  # transition to the wall
#  #h+=hull()(cube((d2+w,eps,hook_height)),
#  #            back(2*w)(cube((d2+w,eps,eps))))
#  h+=hull()(translate((-d2-w,0,0))(cube((d+2*w,eps,hook_height))),
#              translate((-d2-w,-2*w,0))(cube((d+2*w,eps,eps))))

  return h

mesh=True
show_hook=False

def hook_pocket():
  w=hook_width
  d=hook_opening
  big=10*w
  h=rotate((90,0,0))(cylinder(r=w+hwall_thickness,h=hook_lip+hwall_thickness)-
      translate((-w-hwall_thickness-eps,-big,-eps))(
        cube((2*(w+hwall_thickness+eps),big,hook_lip+hwall_thickness+2*eps))))
  h=hull()(h,back(hook_lip+5*w)(sphere(eps)))
  h-=translate((-w/2-hook_gap,-hook_lip+eps,-eps))(cube((w+2*hook_gap,hook_lip,w+2*hook_gap)))
  h=left((d+w)/2)(h)
  return h


  c=w+hook_width_tol
  c2=c+hwall_thickness
  x0=-d/2-4*w-3*hook_gap
  y0=-2*hook_lip-hook_tol-2*w
  h=translate((x0,y0))(cube((w,hook_lip*2+hook_tol+2*w,c)))
  #h+=translate((-d/2-2*w-hook_gap,-hook_lip+w,0))(
  #  hull()(
  #    cube((w,hook_lip-w,c)),
  #    back(w)(cube((w,eps,eps)))))
  h+=translate((-d/2-2*w-hook_gap,-hook_lip,0))(cube((w,hook_lip-w,c)))
  h+=translate((x0,y0-eps))(     # bottom chamfer
    hull()(cube((3*w+2*hook_gap,eps,c2)),
           back(2*c)(cube((3*w+2*hook_gap,eps,eps)))
    ))
  h+=translate((x0,y0-eps,c-eps))(cube((3*w+2*hook_gap,-y0,hwall_thickness)))  # lid
  return h


def body():
  #return color((0,0,1.,0.5))(hook())+color((1.,0,0,1))(hook_pocket())
  #return hook_pocket()
  # hollow the basket
  inside=solidbody(swall_thickness,fwall_thickness)
  b=solidbody(0,0)-up(bwall_thickness)(inside)
  #b=b+color(Green)(grid())
  # we need different versions of the walls just to avoid singularities
  bottom=down(eps)(solidbody(swall_thickness+rim+eps,fwall_thickness+rim+eps))-up(bwall_thickness+2*eps)(inside)
  # hollow the bottom and add the grid
  if mesh:
         g=grid()
         b=b-bottom+g*enlarge(bottom)
  # side grids
         sidex=length-2*corner_radius
         side=translate((-0.5*sidex,-0.5*width-eps,bwall_thickness+extra_topbottom))(cube((sidex,fwall_thickness+2*eps,height-2*bwall_thickness-2*extra_topbottom)))
         # cut out both sides
         b-=side
         side2=forward(width-fwall_thickness)(side)
         b-=side2
         # add the grid
         g2=down(2)(back(0.5*width)(rotate((90,45,0))(g)))*enlarge(side)
         b+=g2
         b+=forward(width-fwall_thickness)(g2)
  # add the hooks
  #ht=translate((0.5*length-swall_thickness+eps,0,height))(rotate((90,0,90))(hook_pocket()))
  if show_hook:
    ht=translate((0.5*length-eps,0,height))(rotate((90,0,90))(
      color((1,0,0,0.5))(hook())+color((0,0,1,0.5))(hook_pocket())))
  else:
    ht=translate((0.5*length-eps,0,height))(rotate((90,0,90))(hook_pocket()))

  #b+=ht
  b+=ht+mirror((1,0,0))(ht)
  
  return b


def render_to_file(object,filename):
  file_out = scad_render_to_file(object, file_header=f'$fn = {SEGMENTS};', filepath=filename, include_orig_code=False)
  print(f"{__file__}: SCAD file written to: {file_out}")

if __name__ == '__main__':  
  render_to_file(body(),"dwtbasket.scad")
  render_to_file(hook(),"hook.scad") # to be printed twice
