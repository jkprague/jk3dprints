# Carpenter template
# to drill holes into a wooden beam
# Cadquery 2.0 script
# Jan Kybic, kybic@fel.cvut.cz, 2020

w=3 # wall width
h=20 # horizontal offset
v=10 # vertical gap
d=30 # lip depth
l=30 # vertical length
z=10 # depth

# base plate
b=cq.Workplane("XY").box(l+w,h+w,z+w,centered=(False,False,False))
b=b.cut(b.translate((w,w,w)))
b=b.center(-v+w,h).box(v,w,d,centered=(False,False,False))
b=b.faces(">Z").chamfer(0.5)
b=b.translate((0.5*v-w,0,0)) # make the center at 0
b=b.union(b.mirror(mirrorPlane="YZ"))


show_object(b)
#show_object(b2)
#show_object(b3)
with open('template2.stl','w') as f:
    f.write(cq.exporters.toString(b,'STL',0.01))
f.close()

