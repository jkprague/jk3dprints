# Carpenter template
# to drill holes into a wooden beam
# Cadquery 2.0 script
# Jan Kybic, kybic@fel.cvut.cz, 2020

w=3 # wall width
l=95 # beam width
d=15 # distance of the first hole from the edge
e=8 # distance between holes
r=4 # hole radius
f=20+19 # distance from the right edge to the axis
g=10    # distance from the left edge to the axis
h=10    # rim height

# base plate
b=cq.Workplane("XY")
b=b.rect(f+g,l).extrude(w)
#
#b=b.edges().chamfer(1)
# holes
b=b.faces("<Z").workplane().center(0.5*(g-f),0)
b=b.pushPoints([(0,0.5*l-d),(0,0.5*l-d-e),(0,-0.5*l+d),(0,-0.5*l+d+e)]).hole(r)
# right edge
b2=cq.Workplane("XY",origin=(0.5*(f+g+w),0)).rect(w,l).extrude(h,False)
# bottom edge
b3=cq.Workplane("XY",origin=(0.5*w,0.5*(l+w))).rect(f+g+w,w).extrude(h,False)
b=b.union(b2).union(b3)

show_object(b)
#show_object(b2)
#show_object(b3)
with open('template1.stl','w') as f:
    f.write(cq.exporters.toString(b,'STL',0.01))
f.close()

