# A lever for a valve
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

rim=3
outerd=12
height=6
length=50
screwhole=3.6
shaftd=7.5
shafth=5.3
# insert width 5.3, desired internal dimension of the plastic part 5.4
# when shaftw=5.8, real internal width is 5.7
shaftw=5.5
cornerr=2
chamfers=0.5
stoph=3
stopofs=2

b=cq.Workplane("XY").rect(length,outerd).extrude(height)
# the cylinder part
b=b.faces("<Z").workplane(offset=rim).moveTo(-0.5*length,0)
s=b.moveTo(-0.5*length+0.25*outerd+stoph-stopofs,-0.25*outerd-stoph+stopofs).rect(0.5*outerd+2*stoph,0.5*outerd+2*stoph).extrude(-rim,False)
c=b.moveTo(-0.5*length,0).circle(0.5*outerd+stoph).extrude(-rim,False)
sel = cq.selectors.StringSyntaxSelector
c=c.faces(sel(">Z")+sel("<Z")).edges().chamfer(0.49*rim)
c=c.cut(s)
#b=b.faces("<Z").workplane(offset=rim).moveTo(-0.5*length,0).line(-stopofs,-0.5*outerd-stoph).lineTo(+0.5*outerd+stoph,stopofs).close().extrude(-rim)
b=b.moveTo(-0.5*length,0).circle(0.5*outerd).extrude(-height-rim)
b=b.edges("|Z").fillet(cornerr)
b=b.faces(">Z").edges().chamfer(chamfers)
#b=b.faces(cq.NearestToPointSelector((0,0,-1))).edges("#Z").chamfer(chamfers)
#b=b.faces("<Y").edges().chamfer(chamfers)
#b=b.faces(cq.NearestToPointSelector((0,0,-1))).edges("|X").chamfer(0.01*chamfers)
#b=b.edges("#Z").chamfer(chamfers)
b=b.union(c)
#b=b.faces("#Z").edges("#Z").chamfer(chamfers)
#b=b.faces("#Z").edges().chamfer(chamfers)
# the attachment hole
h=b.faces("<Z").moveTo(-0.5*length,0).circle(0.5*shaftd).extrude(-shafth,False)
r=b.faces("<Z").moveTo(-0.5*length,0).rect(shaftd,shaftw).extrude(-shafth,False)
h=h.intersect(r)
# hole for the screw
b=b.faces("<Z").moveTo(-0.5*length,0).circle(0.5*screwhole).cutThruAll()
b=b.cut(h)

show_object(b)

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(b,"lever.stl")
