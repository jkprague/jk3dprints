# A bike bottle cap
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

# big circle
d1=18
w1=3
t1=1.5 # thickness

strapw=5 # strap width and length
#strapl=30 
strapl=60

d2=22.5 # inner cup diameter
#d2=22.2 # inner cup diameter
w2=1.5  # cup wall thickness


d3=d2+2*w2 # outer cup diameter
rim=0.5      # rim width
rimofs=5     # rim vertical offseet
h1=10      # inner cup height, not counting the rim and the roof
#chamf=4    # chamfer around the top of the cup


# strap around the bottle
t=cq.Workplane("XY").circle(0.5*d1+w1).circle(0.5*d1).extrude(t1)
#t=t.center(0,0.5*r1+w1).rect(w1,1).extrude(5)
# strip
t=t.center(0,0.5*d1+w1*0.5+0.5*strapl).rect(strapw,strapl).extrude(t1)
# cup
s=cq.Workplane("YZ",origin=(0,strapl+0.5*d1+0.5*d3+0.5*w1-0.5*w2,0))
# draw the profile
#s=s.moveTo(-0.5*d3,0).line(w2,0).line(rim,rim).line(0,rim).line(-rim,rim).line(0,h1)
#s=s.line(0.5*d2,0.5*d2).line(0,w2).line(-0.5*d3+chamf,0).lineTo(-0.5*d3,h1+3*rim+0.5*d2+w2-chamf)
s=s.moveTo(-0.5*d3,0).line(0.5*d3,0).line(0,w2).line(-0.5*d2,0).line(0,rimofs)
s=s.line(rim,rim).line(0,rim).line(-rim,rim)
s=s.line(0,h1-rimofs-3*rim)
s=s.line(rim,rim).line(0,rim).line(-rim,rim)
s=s.line(-w2,0)
#s=s.line(0.5*d2,0).line(0,w2).line(-0.5*d3,0).lineTo(-0.5*d3,h1+3*rim+w2)
s=s.close().revolve()


t=t.union(s)
#show_object(s)
show_object(t)

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(t,"kryt_lahev.stl")

