# A box for Raspberry PI zero with a relay
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq
import math

rplength=65 # Raspberry Pi W length and width
rpwidth=30
rpsdgap=4 # gap where the SD card is
rpgap=2 # gap arround Raspberry on all other sides
wall=2  # wall thickness
rpabove=6 # needed space above the bottom edge of the RPi 
#bottomw=2 # bottom thickness

relbot=3 # space under the relay board
relboard=1.5 # relay board thickness
holehab=14 # hole height above relay board
holediam=6.5 # hole for the cable

between=23 # gap between the two boards

corner_radius=1
lip_height=5
gap=0.2 # horizontal gap between the top part and the lip

chamfer_size=1
lip_chamfer=0.3
eps=1e-3
lip_offset=wall+gap
tooth_length=10 # the tooth/indendation for locking
tooth_offset=2 # how far from the top lip edgee
tooth_height=0.5 # how much should the tooth protrude
tooth_width=2 # vertical dimension


# height of the bottom part
bottom=wall+relbot+relboard+holehab-0.5*holediam
# calculate outer dimensions
length=rplength+rpsdgap+rpgap*2+wall*2
width=rpwidth+2*rpgap+2*wall

# create the bottom part as a solid
bwp=cq.Workplane("XY")
b=bwp.rect(length,width).extrude(bottom)
#b=b.edges("|Z").fillet(corner_radius)
#b=b.faces(">Z").shell(-wall)
# add the lip
l=b.faces(">Z").workplane().rect(length-2*lip_offset,width-2*lip_offset).extrude(lip_height,False)
#l=l.edges("|Z").fillet(corner_radius-lip_offset)
#sel = cq.selectors.StringSyntaxSelector
#l=l.faces(sel(">Z")+sel("<Z")).shell(wall)
b=b.union(l)
b=b.faces(">Z").shell(-wall)
# shell out and chamfer
b=b.edges("|Z").fillet(corner_radius)
b=b.faces(">Z").chamfer(lip_chamfer)
b=b.faces("<Z").edges().chamfer(chamfer_size)
#b=b.edges("|Z").chamfer(chamfer_size-eps)

# the top cover
height=2*wall+relbot+relboard+between+rpabove # total height of the box
cover_height=height-bottom
c=cq.Workplane("XY").workplane(offset=height).rect(length,width).extrude(-cover_height)
c=c.faces("<Z").shell(-wall)
c=c.edges("|Z").fillet(corner_radius)
c=c.faces(">Z").chamfer(chamfer_size)

# a simple the tooth
t=b.faces("<Y").workplane(offset=-lip_offset).transformed(offset=(0,0.5*bottom+lip_height-tooth_offset))
t=t.rect(tooth_length,tooth_width).extrude(tooth_height,False)
t=t.faces("<Y").chamfer(tooth_height-eps)
b=b.union(t).union(t.mirror("XZ"))

# cut the dent to the cover
ct=t.translate((0,-gap,0))
c=c.cut(ct).cut(ct.mirror("XZ"))

# support pin on a workplace
def support_pin(b,outerd,innerd,bottomheight,topheight):
  b1=b.circle(0.5*outerd).extrude(bottomheight,False)
  b=b.workplane(offset=bottomheight).circle(0.5*innerd).extrude(topheight)
  #b=b.circle(0.5*innerd).extrude(topheight)
  b=b.union(b1)
  return b

relholedist1=47
relholedist2=22
relpininner=3
relpinouter=6
relpinheight=5

rpiholedist1=58
rpiholedist2=23
rpipininner=2.5
rpipinouter=5
rpipinheight=5


# shift everything a little to make space for the SD card

# make four pins for the RPi board
pts=[(-rpiholedist1,-rpiholedist2),(rpiholedist1,-rpiholedist2),(rpiholedist1,rpiholedist2),
       (-rpiholedist1,rpiholedist2)]
bwp=cq.Workplane("XY").workplane(offset=wall)
pin=support_pin(bwp,rpipinouter,rpipininner,relbot+relboard+between,rpipinheight)
for p in pts:
  b=b.union(pin.translate((p[0]*0.5+0.5*(rpsdgap-rpgap),p[1]*0.5)))


# make four pins for the relay board
pts=[(-relholedist1,-relholedist2),(relholedist1,-relholedist2),(relholedist1,relholedist2),
       (-relholedist1,relholedist2)]
bwp=cq.Workplane("XY").workplane(offset=wall)
pin=support_pin(bwp,relpinouter,relpininner,relbot,relpinheight)
for p in pts:
  b=b.union(pin.translate((p[0]*0.5+0.5*(rpsdgap-rpgap),p[1]*0.5)))

# hole for the cable
holeposy=wall+relbot+relboard+holehab
holeposx=-20
h1=b.faces(">Y").workplane(centerOption="ProjectedOrigin").center(holeposx,holeposy)
h1=h1.circle(0.5*holediam).extrude(-width,False)
h1=h1.center(0,0.5*holediam).rect(holediam,holediam).extrude(-width)
b=b.cut(h1)
h2=b.faces(">Y").workplane(centerOption="ProjectedOrigin").center(holeposx,holeposy)
h2=h2.polygon(6,holediam).extrude(-width,False)
h2=h2.center(0,-0.5*holediam).rect(holediam,holediam).extrude(-width)
c=c.cut(h2)

# hole for the micro USB connector
usbwidth=12
usbheight=6
usbposy=wall+relbot+relboard+between+3 # vertical position of the center
usbposx=0.5*rplength-11+0.5*(rpsdgap-rpgap)
h3=b.faces("<Y").workplane(centerOption="ProjectedOrigin").center(usbposx,usbposy)
h3=h3.rect(usbwidth,usbheight).extrude(-2*wall,False)
h3=h3.edges("|Y").chamfer(0.5)
c=c.cut(h3)
show_object(b,name="bottom")
#show_object(h1,name="holeb")
#show_object(h2,name="holec")
show_object(h3,name="hole3")
show_object(c,name="cover")




def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(b,"rpirboxbottom.stl")
write_object(c,"rpirboxtop.stl")

