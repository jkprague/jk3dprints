# Big box with lid
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020

import cadquery as cq
import math

length=240
width=205
height=200
thickness=1.5
gap=1 
chamfer_size=1
lip_chamfer=0.3

cover_height=15
top_gap=1
corner_radius=20

eps=1e-3

lip_height=cover_height - thickness - top_gap
bottom_height=height-cover_height
lip_offset=thickness+gap

# create the bottom part as a solid
b=cq.Workplane("XY").rect(length,width).extrude(bottom_height)
b=b.edges("|Z").fillet(corner_radius)
# lip as a solid
l=b.faces(">Z").workplane().rect(length-2*lip_offset,width-2*lip_offset).extrude(lip_height,False)
l=l.edges("|Z").fillet(corner_radius-lip_offset)
b=b.union(l)
# shell out
b=b.faces(">Z").shell(-thickness)
#b=b.faces("#Z").fillet(-1)
b=b.faces("<Z").edges().chamfer(chamfer_size)
b=b.faces(">Z").chamfer(lip_chamfer)
#b=(b.faces(">Z").workplane(centerOption="CenterOfMass").transformed(rotate=(0,0,0),offset=(0,0))
#     .text("14.2.2020",8,0.5,cut=False,combine=True,font="Verdana",kind='regular'))

# create the cover
c=cq.Workplane("XY").workplane(offset=height).rect(length,width).extrude(-cover_height)
c=c.edges("|Z").fillet(corner_radius)
c=c.faces("<Z").shell(-thickness)
c=c.faces(">Z").chamfer(chamfer_size)
#c=(c.faces(">X").workplane(centerOption="CenterOfMass").transformed(rotate=(0,0,0),offset=(0,0))
#     .text("BIOODPAD",8,0.5,cut=False,combine=True,font="Verdana",kind='regular'))

#show_object(c,name="cover")

#show_object(b,name="bottom")


# create the cover
c2=cq.Workplane("XY").workplane(offset=height).rect(length+2*(thickness+gap),width+2*(thickness+gap)).extrude(-cover_height)
c2=c2.edges("|Z").fillet(corner_radius)
c2=c2.faces("<Z").shell(-thickness)
c2=c2.faces(">Z").chamfer(chamfer_size)
show_object(c2,name="cover2")



# The handle
hheight=20
hrise=15
hlength=50
hwidth=8
chamfer=2
sag=0.2
hpts = [(0, 0), (0,0.3*hheight),(hrise, hheight), (hrise+hlength, hheight),(hlength+2*hrise,0.3*hheight),(hlength+2*hrise,0)]
helement=[(hwidth-chamfer,-hwidth),(hwidth, -hwidth+chamfer),
            (hwidth,hwidth-chamfer),(hwidth-chamfer,hwidth),
            (-hwidth+chamfer,hwidth),(-hwidth,hwidth-chamfer),
            (-hwidth,-hwidth+chamfer),(-hwidth+chamfer,-hwidth)]
hpath=cq.Workplane("XZ").moveTo(0,0).lineTo(0,0.3*hheight)
hpath=hpath.sagittaArc((hrise,hheight),sag*hheight).line(hlength,0)
hpath=hpath.sagittaArc((2*hrise+hlength,0.3*hheight),sag*hheight).lineTo(hlength+2*hrise,0)
#hpath=cq.Workplane("XZ").polyline(hpts)
#hpath=cq.Workplane("XZ").moveTo(0,0).line(0,0.3*hheight).
#h=cq.Workplane("XY").rect(hwidth,hwidth).sweep(hpath,isFrenet=True)
h=cq.Workplane("XY").polyline(helement).close()
h=h.sweep(hpath)
#h=h.edges("|Y").fillet(3)
show_object(h.translate((-0.5*hlength-hrise,0,height)),name="handle")

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(b,"bigboxbottom.stl")
write_object(c,"bigboxcover.stl")
write_object(h,"bigboxhandle.stl")
write_object(c2,"bigboxcover2.stl")
