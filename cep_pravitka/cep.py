# cep pravitka
# Cadquery 2.0 script
# https://cadquery.readthedocs.io/
# Jan Kybic, kybic@fel.cvut.cz, 2020


ds=4 # shaft diameter
de=16 # external knob diameter
dg=0.2 # gap
te=3  # external knob thickness
ti=1  # pin inset height
tp=4  # net pin height

# bottom part with pin
t=cq.Workplane("XY").circle(0.5*de).extrude(te)
t=t.faces(">Z").workplane().circle(0.5*ds).extrude(tp+ti)

s=cq.Workplane("XY").circle(0.5*de).extrude(te)
s=s.faces(">Z").workplane().circle(0.5*ds+dg).cutBlind(-ti)

show_object(t,name="bottom")
st=s.rotate((1,0,0),(0,0,0),180).translate((0,0,tp+2*te))
#.translate((50,0,0))
show_object(st,name="top")

def write_object(o,filename):
  with open(filename,'w') as f:
    f.write(cq.exporters.toString(o,'STL',0.01))
    print("Written file: ",filename)
  f.close()

write_object(t,"cep1.stl")
write_object(s,"cep2.stl")
